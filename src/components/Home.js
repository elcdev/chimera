import React, { Component } from 'react';
import { Jumbotron, Button } from 'reactstrap';

class Home extends Component {
    render() {
        return (
            <React.Fragment>
                <Jumbotron>
                    <div className='container'>
                        <h1 className="display-3">Welcome to Chimera!</h1>
                        <p className="lead">This is a simple hero unit, a simple Jumbotron-style component for calling extra attention to featured content or information.</p>
                        <hr className="my-2" />
                        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                        <p className="lead">
                            <Button color="primary">Learn More</Button>
                        </p>
                    </div>
                </Jumbotron>
                <div className='container'>
                    Home page cotent
            </div>
            </React.Fragment>

        )
    }
}

export default Home;